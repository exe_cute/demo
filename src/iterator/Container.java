package iterator;


public class Container implements iter {

	public String names[]={};
	int index;
	
	
	@Override
	public boolean hasnext() {
		if(index<names.length){
			return true;
		}
		return false;
	}
	@Override
	public Object next() {

		if(this.hasnext())
		{
			return names[index++];
		}
		return null;
	}
	public void add(String o){
		names[index++]=o;
	}
	
	
}
